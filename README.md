# AlumniNetwork

This is the backend part of a larger project, created using Asp.net. The idea was to create a forum with communication through discussions, events and groups. A platform where people interested in the same topics are able to connect.

## Install
Clone the repository and open the solution using the sln file.
To run the project you would have to seed the database by typing the following command in the "Package Manager Console" in Visual Studio.
```
update-database -context alumnicontext
```
Done! Now you can simply run the project through Visual Studio, a web API server and a browser with Swagger will open automatically.

## Authors
#### [Johan Lantz (@johan.lantz.a)](@johan.lantz.a)
#### [Pontus Gillson (@d0senb0den)](@d0senb0den)
#### [Erik Morling (@ermo1222)](@ermo1222)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
