﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using AlumniNetworkAPI.Models;

namespace AlumniNetworkAPI.Data
{
    public class AlumniContext : DbContext
    {
        public DbSet<Event> Events { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<RSVP> RSVPs { get; set; }
        public DbSet<DiscussionThread> Threads { get; set; }
        public DbSet<Topic> Topics { get; set; }
        public DbSet<User> Users { get; set; }



        public readonly IConfiguration _configuration;

        public AlumniContext(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("AlumniDB"));
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 1,
                    Name = "Pontus",
                    Bio = "Loud keyboard and a bit of a noob dying to Lantz",
                    FunFact = "Ouch",
                    Img = "afdsa",
                    Status = "works at experis"
                });
            modelBuilder.Entity<User>()
                .HasData(new User
                {
                    Id = 2,
                    Name = "Pontus Alt Acc",
                    Bio = "Hmmm",
                    FunFact = "Tarkov fanatic",
                    Img = "afdsa",
                    Status = "Wants to work at Bits Data",        
                });

            modelBuilder.Entity<DiscussionThread>()
                .HasData(new DiscussionThread
                {
                    Id = 1,
                    CreatedBy = 1, 
                    LastUpdatedAt = DateTime.UtcNow,
                    TopicId = 1,
                    CreatedAt = DateTime.UtcNow,
                    IsEvent = false
                });

            modelBuilder.Entity<Post>()
                .HasData(new Post
                {
                    Id =1,
                    ThreadId = 1,
                    CreatedBy = 1,
                    LastUpdatedAt= DateTime.UtcNow,
                    IsTopLevel = true,
                });
            modelBuilder.Entity<Topic>()
                .HasData(new Topic
                {
                    Id = 1,
                    Description = "Discussions about tarkov",
                    Name = "Tarkov",
                    
                });
            modelBuilder.Entity<Group>()
                .HasData(new Group
                {
                    Id=1,
                    Name =  "Meh Tarkov Party",
                    IsPrivate = false,
                    Description = "Group who plays tarkov at a meh level"
                });

            modelBuilder.Entity<Event>()
                .HasData(new Event
                {
                    Id = 1,
                    BannerImg = "ASDF",
                    CreatedAt = DateTime.UtcNow,
                    End = DateTime.UtcNow,
                    IsPrivate = false,
                    Name = "SickkEvenntt",
                    GuestCount = 2,
                    LastUpdatedAt = DateTime.UtcNow,
                    ThreadId = 1,
                    Description = "A temp sicck event to test seeding",
                    Start = DateTime.UtcNow,
                });

            modelBuilder.Entity<RSVP>()
                .HasData(new RSVP
                {
                    Id = 1,
                    EventId = 1,
                    UserId = 1,
                });
            modelBuilder.Entity<RSVP>()
                .HasData(new RSVP
                {
                    Id = 2,
                    EventId = 1,
                    UserId = 2,
                });


            modelBuilder.Entity<User>()
               .HasMany(p => p.Groups)
               .WithMany(b => b.Users)
               .UsingEntity<Dictionary<string, object>>(
               configureRight => configureRight.HasOne<Group>().WithMany().HasForeignKey("GroupId"),
               configureLeft => configureLeft.HasOne<User>().WithMany().HasForeignKey("UserId"),
               joiningEntities =>
               {
                   joiningEntities.HasKey("UserId", "GroupId");
                   joiningEntities.HasData(
                       new { UserId = 1, GroupId = 1 },
                       new { UserId = 2, GroupId = 1 }
                       );
               });

            modelBuilder.Entity<User>()
               .HasMany(p => p.Topics)
               .WithMany(b => b.Users)
               .UsingEntity<Dictionary<string, object>>(
               configureRight => configureRight.HasOne<Topic>().WithMany().HasForeignKey("TopicId"),
               configureLeft => configureLeft.HasOne<User>().WithMany().HasForeignKey("UserId"),
               joiningEntities =>
               {
                   joiningEntities.HasKey("UserId", "TopicId");
                   joiningEntities.HasData(
                       new { UserId = 1, TopicId = 1 },
                       new { UserId = 2, TopicId = 1 }
                       );
               });

        } 
    }
}
