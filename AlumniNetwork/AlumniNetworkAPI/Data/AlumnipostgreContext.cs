﻿using Microsoft.EntityFrameworkCore;

namespace AlumniNetworkAPI.Data
{
    public class AlumnipostgreContext : AlumniContext
    {
        public AlumnipostgreContext(IConfiguration configuration) : base(configuration) { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseNpgsql(_configuration.GetConnectionString("public"));
        }
    }
}
