﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.DiscussionThreadDTO
{
    public class DTO_ReadDiscussionThread
    {
        [Required]
        public int? CreatedBy { get; set; }
        public int? TopicId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public bool? IsEvent { get; set; }
        public virtual List<int>? Posts { get; set; }
    }
}
