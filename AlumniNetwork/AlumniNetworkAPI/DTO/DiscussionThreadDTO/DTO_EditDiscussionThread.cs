﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.DiscussionThreadDTO
{
    public class DTO_EditDiscussionThread
    {
        public int DTO_ID { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public bool? IsEvent { get; set; }
        public virtual List<int>? Posts { get; set; }
    }
}
