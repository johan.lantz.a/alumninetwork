﻿using AlumniNetworkAPI.DTO.PostDTO;
using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.DiscussionThreadDTO
{
    public class DTO_CreateDiscussionThread
    {
        [Required]
        public int? CreatedBy { get; set; }
        public int? TopicId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public bool? isEvent { get; set; }
        public DTO_CreatePost? TopLevelPost { get; set; }
    }
}
