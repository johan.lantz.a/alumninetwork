﻿namespace AlumniNetworkAPI.DTO.RsvpDTO
{
    public class DTO_ReadRSVP
    {
        public int EventId { get; set; }
        public int UserId { get; set; }
        public DateTime? LastUpdated { get; set; }
        public bool Accepted { get; set; }
    }
}
