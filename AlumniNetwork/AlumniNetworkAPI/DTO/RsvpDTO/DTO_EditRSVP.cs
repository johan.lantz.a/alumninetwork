﻿namespace AlumniNetworkAPI.DTO.RsvpDTO
{
    public class DTO_EditRSVP
    {
        public int DTO_ID { get; set; }
        public DateTime? LastUpdated { get; set; }
        public bool Accepted { get; set; }
    }
}
