﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.UserDTO
{
    public class DTO_CreateUser
    {
        [StringLength(100)]
        public string? Name { get; set; }
        [StringLength(300)]
        public string? Img { get; set; }
        [StringLength(300)]
        public string? Status { get; set; }
        [StringLength(500)]
        public string? Bio { get; set; }
        [StringLength(500)]
        public string? FunFact { get; set; }
        //public virtual List<int>? Topics { get; set; }
        //public virtual List<int>? Groups { get; set; }
        //public virtual List<int>? RSVPs { get; set; }
    }
}
