﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.TopicDTO
{
    public class DTO_ReadTopic
    {
        [MaxLength(200)]
        public string? Name { get; set; }

        [MaxLength(500)]
        public string? Description { get; set; }

        public virtual List<int>? Users { get; set; }
    }
}
