﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.EventDTO
{
    public class DTO_CreateEvent
    {
        public int ThreadId { get; set; }
        [MaxLength(200)]
        public string? Name { get; set; }
        [MaxLength(500)]
        public string? Description { get; set; }
        [MaxLength(300)]
        public string? BannerImg { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public bool IsPrivate { get; set; }
        public int? GroupId { get; set; }
    }
}
