﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.PostDTO
{
    public class DTO_ReadPost
    {
        [Required]
        public int? CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public bool? IsTopLevel { get; set; }
        public int? ThreadId { get; set; }
        public int? ReplyingTo { get; set; }

        [StringLength(1000)]
        public string Text { get; set; }
    }
}
