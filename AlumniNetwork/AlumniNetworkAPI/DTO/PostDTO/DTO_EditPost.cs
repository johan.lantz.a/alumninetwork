﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.PostDTO
{
    public class DTO_EditPost
    {
        public int DTO_ID { get; set; }
        public DateTime? LastUpdatedAt { get; set; }

        [StringLength(1000)]
        public string Text { get; set; }
    }
}
