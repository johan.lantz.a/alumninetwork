﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.DTO.GroupDTO
{
    public class DTO_ReadGroup
    {
        [StringLength(200)]
        public string? Name { get; set; }
        [StringLength(500)]
        public string? Description { get; set; }
        public bool IsPrivate { get; set; }
        public virtual List<int>? Users { get; set; }
        public virtual List<int>? Events { get; set; }
    }
}
