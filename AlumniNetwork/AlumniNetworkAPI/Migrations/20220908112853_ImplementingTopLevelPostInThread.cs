﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetworkAPI.Migrations
{
    public partial class ImplementingTopLevelPostInThread : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TopLevelPostID",
                table: "Threads",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6461), new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6465), new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6467) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6432));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6418), new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6369) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TopLevelPostID",
                table: "Threads");

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(61), new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(64), new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(65) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(29));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(16), new DateTime(2022, 9, 6, 13, 52, 0, 996, DateTimeKind.Local).AddTicks(9991) });
        }
    }
}
