﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetworkAPI.Migrations
{
    public partial class SomeValuesInModelsNowNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RSVPs_Events_EventId",
                table: "RSVPs");

            migrationBuilder.DropForeignKey(
                name: "FK_RSVPs_Users_UserId",
                table: "RSVPs");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "RSVPs",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "EventId",
                table: "RSVPs",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6972), new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6976), new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6977) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6945));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6933), new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6909) });

            migrationBuilder.AddForeignKey(
                name: "FK_RSVPs_Events_EventId",
                table: "RSVPs",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RSVPs_Users_UserId",
                table: "RSVPs",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RSVPs_Events_EventId",
                table: "RSVPs");

            migrationBuilder.DropForeignKey(
                name: "FK_RSVPs_Users_UserId",
                table: "RSVPs");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "RSVPs",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "EventId",
                table: "RSVPs",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3800), new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3806), new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3809) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3752));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3729), new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3660) });

            migrationBuilder.AddForeignKey(
                name: "FK_RSVPs_Events_EventId",
                table: "RSVPs",
                column: "EventId",
                principalTable: "Events",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_RSVPs_Users_UserId",
                table: "RSVPs",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
