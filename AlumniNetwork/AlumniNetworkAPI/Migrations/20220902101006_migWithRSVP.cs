﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetworkAPI.Migrations
{
    public partial class migWithRSVP : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8974), new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8978), new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8980) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8946));

            migrationBuilder.InsertData(
                table: "RSVPs",
                columns: new[] { "Id", "EventId", "LastUpdated", "UserId" },
                values: new object[,]
                {
                    { 1, 1, null, 1 },
                    { 2, 1, null, 2 }
                });

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8931), new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8904) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "RSVPs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "RSVPs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6124), new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6128), new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6130) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6060));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6047), new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6020) });
        }
    }
}
