﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetworkAPI.Migrations
{
    public partial class FixNullableForDatabaseVariables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Threads_ThreadId",
                table: "Events");

            migrationBuilder.AlterColumn<int>(
                name: "ThreadId",
                table: "Events",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(61), new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(64), new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(65) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(29));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 6, 13, 52, 0, 997, DateTimeKind.Local).AddTicks(16), new DateTime(2022, 9, 6, 13, 52, 0, 996, DateTimeKind.Local).AddTicks(9991) });

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Threads_ThreadId",
                table: "Events",
                column: "ThreadId",
                principalTable: "Threads",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Events_Threads_ThreadId",
                table: "Events");

            migrationBuilder.AlterColumn<int>(
                name: "ThreadId",
                table: "Events",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6972), new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6976), new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6977) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6945));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6933), new DateTime(2022, 9, 5, 15, 38, 47, 158, DateTimeKind.Local).AddTicks(6909) });

            migrationBuilder.AddForeignKey(
                name: "FK_Events_Threads_ThreadId",
                table: "Events",
                column: "ThreadId",
                principalTable: "Threads",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
