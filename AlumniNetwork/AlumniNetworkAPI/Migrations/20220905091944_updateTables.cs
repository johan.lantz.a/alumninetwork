﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetworkAPI.Migrations
{
    public partial class updateTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "isEvent",
                table: "Threads",
                newName: "IsEvent");

            migrationBuilder.AddColumn<bool>(
                name: "Accepted",
                table: "RSVPs",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Text",
                table: "Posts",
                type: "nvarchar(1000)",
                maxLength: 1000,
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3800), new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3806), new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3809) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3752));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3729), new DateTime(2022, 9, 5, 11, 19, 43, 611, DateTimeKind.Local).AddTicks(3660) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Accepted",
                table: "RSVPs");

            migrationBuilder.DropColumn(
                name: "Text",
                table: "Posts");

            migrationBuilder.RenameColumn(
                name: "IsEvent",
                table: "Threads",
                newName: "isEvent");

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8974), new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8978), new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8980) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8946));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8931), new DateTime(2022, 9, 2, 12, 10, 6, 536, DateTimeKind.Local).AddTicks(8904) });
        }
    }
}
