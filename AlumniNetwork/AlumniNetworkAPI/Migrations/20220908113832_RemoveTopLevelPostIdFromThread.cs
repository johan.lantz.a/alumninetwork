﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetworkAPI.Migrations
{
    public partial class RemoveTopLevelPostIdFromThread : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TopLevelPostID",
                table: "Threads");

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 8, 13, 38, 31, 907, DateTimeKind.Local).AddTicks(5003), new DateTime(2022, 9, 8, 13, 38, 31, 907, DateTimeKind.Local).AddTicks(5007), new DateTime(2022, 9, 8, 13, 38, 31, 907, DateTimeKind.Local).AddTicks(5009) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 8, 13, 38, 31, 907, DateTimeKind.Local).AddTicks(4927));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 8, 13, 38, 31, 907, DateTimeKind.Local).AddTicks(4916), new DateTime(2022, 9, 8, 13, 38, 31, 907, DateTimeKind.Local).AddTicks(4877) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TopLevelPostID",
                table: "Threads",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt", "Start" },
                values: new object[] { new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6461), new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6465), new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6467) });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6432));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6418), new DateTime(2022, 9, 8, 13, 28, 53, 19, DateTimeKind.Local).AddTicks(6369) });
        }
    }
}
