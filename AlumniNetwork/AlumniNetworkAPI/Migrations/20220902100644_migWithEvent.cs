﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace AlumniNetworkAPI.Migrations
{
    public partial class migWithEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Events",
                columns: new[] { "Id", "BannerImg", "CreatedAt", "Description", "End", "GroupId", "GuestCount", "IsPrivate", "LastUpdatedAt", "Name", "Start", "ThreadId" },
                values: new object[] { 1, "ASDF", new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6124), "A temp sicck event to test seeding", new DateTime(2022, 10, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), null, 2, false, new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6128), "SickkEvenntt", new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6130), 1 });

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6060));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6047), new DateTime(2022, 9, 2, 12, 6, 44, 282, DateTimeKind.Local).AddTicks(6020) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Events",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "LastUpdatedAt",
                value: new DateTime(2022, 9, 2, 11, 25, 30, 491, DateTimeKind.Local).AddTicks(7951));

            migrationBuilder.UpdateData(
                table: "Threads",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "LastUpdatedAt" },
                values: new object[] { new DateTime(2022, 9, 2, 11, 25, 30, 491, DateTimeKind.Local).AddTicks(7938), new DateTime(2022, 9, 2, 11, 25, 30, 491, DateTimeKind.Local).AddTicks(7914) });
        }
    }
}
