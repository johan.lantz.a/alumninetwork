﻿using AlumniNetworkAPI.DTO.TopicDTO;
using AlumniNetworkAPI.Models;
using AutoMapper;

namespace AlumniNetworkAPI.Profiles
{
    public class TopicProfile : Profile
    {
        public TopicProfile()
        {
            CreateMap<DTO_CreateTopic, Topic>().ReverseMap();

            CreateMap<DTO_EditTopic, Topic>()
                .ForMember(topic => topic.Id, opt => opt
                .MapFrom(topicDTO => topicDTO.DTO_ID))
                .ForMember(topic => topic.Users, opt => opt
                .MapFrom(topicDTO => topicDTO.Users.Select(userId => new User { Id = userId }).ToList()))
                .ReverseMap();


            CreateMap<Topic, DTO_ReadTopic>()
                .ForMember(topicDTO => topicDTO.Users, opt => opt
                .MapFrom(topic => topic.Users.Select(user => user.Id).ToList()))
                .ReverseMap();
        }
    }
}
