﻿using AlumniNetworkAPI.DTO.GroupDTO;
using AlumniNetworkAPI.Models;
using AutoMapper;

namespace AlumniNetworkAPI.Profiles
{
    public class GroupProfile : Profile
    {
        public GroupProfile()
        {
            CreateMap<DTO_CreateGroup, Group>().ReverseMap();

            CreateMap<Group, DTO_ReadGroup>()
                .ForMember(groupDTO => groupDTO.Events, opt => opt
                .MapFrom(group => group.Events.Select(group => group.Id).ToList()))
                .ForMember(groupDTO => groupDTO.Users, opt => opt
                .MapFrom(group => group.Users.Select(user => user.Id).ToList()))
                .ReverseMap();

            CreateMap<DTO_EditGroup, Group>()
                .ForMember(group => group.Id, opt => opt
                .MapFrom(groupDTO => groupDTO.DTO_Id))
                .ForMember(group => group.Users, opt => opt
                .MapFrom(groupDTO => groupDTO.Users.Select(user => new User { Id = user }).ToList()))
                .ForMember(group => group.Events, opt => opt
                .MapFrom(groupDTO => groupDTO.Events.Select(eventId => new Event { Id = eventId }).ToList()))
                .ReverseMap();

        }
    }
}
