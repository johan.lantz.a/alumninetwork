﻿
using AlumniNetworkAPI.DTO.RsvpDTO;
using AlumniNetworkAPI.Models;
using AutoMapper;

namespace AlumniNetworkAPI.Profiles
{
    public class rsvpProfile : Profile
    {
        public rsvpProfile()
        {
            CreateMap<DTO_CreateRSVP, RSVP>().ReverseMap();

            CreateMap<RSVP, DTO_ReadRSVP>().ReverseMap();
        }
    }
}
