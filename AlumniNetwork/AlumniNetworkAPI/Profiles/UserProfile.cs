﻿namespace AlumniNetworkAPI.Profiles;
using AlumniNetworkAPI.DTO.UserDTO;
using AlumniNetworkAPI.Models;
using AutoMapper;

    public class UserProfile : Profile
    {

        public UserProfile()
        {
 
         CreateMap<DTO_CreateUser, User>().ReverseMap();


        CreateMap<DTO_EditUser, User>()
           .ForMember(user => user.Id, opt => opt
           .MapFrom(userDTO => userDTO.DTO_ID))
           .ForMember(user => user.RSVPs, opt => opt
           .MapFrom(userDTO => userDTO.RSVPs.Select(rsvp => new RSVP { Id = rsvp }).ToList()))
           .ForMember(user => user.Topics, opt => opt
           .MapFrom(userDTO => userDTO.Topics.Select(topic => new Topic { Id = topic }).ToList()))
           .ForMember(user => user.Groups, opt => opt
           .MapFrom(userDTO => userDTO.Groups.Select(group => new Group { Id = group }).ToList()))
           .ReverseMap();


        CreateMap<User, DTO_ReadUser>()
            .ForMember(userDTO => userDTO.Groups, opt => opt
            .MapFrom(user => user.Groups.Select(group => group.Id).ToList()))
            .ForMember(userDTO => userDTO.Topics, opt => opt
            .MapFrom(user => user.Topics.Select(topic => topic.Id).ToList()))
            .ForMember(userDTO => userDTO.RSVPs, opt => opt
            .MapFrom(user => user.RSVPs.Select(rsvp => rsvp.Id).ToList()))
            .ReverseMap();



    }
}

