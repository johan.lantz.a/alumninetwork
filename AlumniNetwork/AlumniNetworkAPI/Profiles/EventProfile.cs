﻿using AlumniNetworkAPI.DTO.EventDTO;
using AlumniNetworkAPI.Models;
using AutoMapper;

namespace AlumniNetworkAPI.Profiles
{
    public class EventProfile : Profile
    {
        public EventProfile()
        {
            CreateMap<DTO_CreateEvent, Event>().ReverseMap();


            CreateMap<DTO_EditEvent, Event>()
                .ForMember(@event => @event.Id, opt => opt
                .MapFrom(eventDTO => eventDTO.DTO_ID))
                .ForMember(@event => @event.RSVPs, opt => opt
                .MapFrom(eventDTO => eventDTO.RSVPs.Select(rsvpId => new RSVP { Id = rsvpId}).ToList()))
                .ReverseMap();


            CreateMap<Event, DTO_ReadEvent>()
                .ForMember(eventDTO => eventDTO.RSVPs, opt => opt
                .MapFrom(@event => @event.RSVPs.Select(rsvp => rsvp.Id).ToList()))
                .ReverseMap();

        }
    }
}
