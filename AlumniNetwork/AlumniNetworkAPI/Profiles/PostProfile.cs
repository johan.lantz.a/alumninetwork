﻿using AlumniNetworkAPI.DTO.PostDTO;
using AlumniNetworkAPI.Models;
using AutoMapper;

namespace AlumniNetworkAPI.Profiles
{
    public class PostProfile : Profile
    {
        public PostProfile()
        {
            CreateMap<DTO_CreatePost, Post>().ReverseMap();

            CreateMap<Post, DTO_ReadPost>().ReverseMap();

        }
    }
}
