﻿using AlumniNetworkAPI.DTO.DiscussionThreadDTO;
using AlumniNetworkAPI.Models;
using AutoMapper;

namespace AlumniNetworkAPI.Profiles
{
    public class DiscussionThreadProfile : Profile
    {
        public DiscussionThreadProfile()
        {
            CreateMap<DTO_CreateDiscussionThread, DiscussionThread>()
                 .ForMember(threadDTO => threadDTO.Posts, opt => opt.Ignore()).ReverseMap();



            CreateMap<DTO_EditDiscussionThread, DiscussionThread>().ReverseMap();


            CreateMap<DiscussionThread, DTO_ReadDiscussionThread>()
                .ForMember(threadDTO => threadDTO.Posts, opt => opt
                .MapFrom(thread => thread.Posts.Select(post => post.Id).ToList()))
                .ReverseMap();
        }

    }
}
