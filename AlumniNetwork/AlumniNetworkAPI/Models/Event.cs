﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.Models
{
    public class Event
    {
        [Required]
        public int Id { get; set; }

        public int? GroupId { get; set; }
        public int? ThreadId { get; set; }
        [MaxLength(200)]
        public string? Name { get; set; }
        [MaxLength(500)]
        public string? Description { get; set; }
        [MaxLength(300)]
        public string? BannerImg { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime LastUpdatedAt { get; set; }

        public int GuestCount { get; set; }
        public bool IsPrivate { get; set; }

        //For connecting tables in database
       
        public virtual ICollection<RSVP>? RSVPs { get; set; }
        public virtual DiscussionThread? Thread { get; set; }
        public virtual Group? Group { get; set; }

    }
}
