﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.Models

{
    public class Group
    {
        [Required]
        public int Id { get; set; }
        [StringLength(200)]
        public string? Name { get; set; }
        [StringLength(500)]
        public string? Description { get; set; }
        public bool IsPrivate { get; set; }
        //For connecting tables in database
        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<Event> Events { get; set; }
    }
}
