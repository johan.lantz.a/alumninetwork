﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.Models
{
    public class DiscussionThread
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int? CreatedBy { get; set; }
       
        //For connecting tables in database
        public virtual User? User { get; set; }

        public int? TopicId { get; set; }

        //For connecting tables in database
        public virtual Topic? Topic { get; set; }

        public DateTime? CreatedAt { get; set; }

        public DateTime? LastUpdatedAt { get; set; }

        public bool? IsEvent { get; set; }

        public virtual ICollection<Post>? Posts { get; set; }


    }
}
