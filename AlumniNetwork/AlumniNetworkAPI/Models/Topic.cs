﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.Models
{
    public class Topic
    {
        [Required]
        public int Id { get; set; }
        [MaxLength(200)]
        public string? Name { get; set; }

        [MaxLength(500)]
        public string? Description { get; set; }

        public virtual ICollection<User> Users { get; set; }

    }
}
