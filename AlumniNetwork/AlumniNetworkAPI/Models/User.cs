﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.Models
{
    public class User
    {
        [Required]
        public int Id { get; set; }
        [StringLength(100)]
        public string? Name { get; set; }
        [StringLength(300)]
        public string? Img { get; set; }
        [StringLength(300)]
        public string? Status { get; set; }
        [StringLength(500)]
        public string? Bio { get; set; }
        [StringLength(500)]
        public string? FunFact { get; set; }
        //For connecting tables in database
        public virtual ICollection<Topic>? Topics { get; set; }
        //For connecting tables in database
        public virtual ICollection<Group>? Groups { get; set; }
        public virtual ICollection<RSVP>? RSVPs { get; set; }

    }
}
