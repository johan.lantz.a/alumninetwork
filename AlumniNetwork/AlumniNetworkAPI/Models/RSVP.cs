﻿namespace AlumniNetworkAPI.Models
{
    public class RSVP
    {
        public int Id { get; set; }
        public int? EventId { get; set; }
        public int? UserId { get; set; }
        public DateTime? LastUpdated { get; set; }
       
        //For connecting tables in database
        public virtual Event? Event { get; set; }
        //For connecting tables in databaseA
        public virtual User? User { get; set; }
        public bool Accepted { get; set; }

    }
}
