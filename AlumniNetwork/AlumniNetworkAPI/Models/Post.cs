﻿using System.ComponentModel.DataAnnotations;

namespace AlumniNetworkAPI.Models
{
    public class Post
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int? CreatedBy { get; set; }
        //For connecting tables in database
        public virtual User? User { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? LastUpdatedAt { get; set; }
        public bool? IsTopLevel { get; set; }
        public int? ThreadId { get; set; }
        //For connecting tables in database
        public virtual DiscussionThread Thread { get; set; }
        public int? ReplyingTo { get; set; }
        [StringLength(1000)]
        public string? Text { get; set; }








        public void CreateNewPost()
        {


            String text = "";

            Post post = new Post() { CreatedAt = DateTime.Now, IsTopLevel = true, };


        }


    }
}
