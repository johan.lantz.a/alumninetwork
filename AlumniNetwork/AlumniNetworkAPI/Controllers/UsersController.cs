﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkAPI.Data;
using AlumniNetworkAPI.Models;
using AutoMapper;
using AlumniNetworkAPI.DTO.UserDTO;
using Microsoft.AspNetCore.Cors;

namespace AlumniNetworkAPI.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly AlumniContext _context;
        private readonly IMapper _mapper;

        public UsersController(AlumniContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Users
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO_ReadUser>>> GetUsers()
        {
          if (_context.Users == null)
          {
              return NotFound();
          }
            return _mapper.Map<List<DTO_ReadUser>>(await _context.Users.Include(user => user.Topics)
               .Include(user => user.RSVPs)
               .Include(user => user.Groups)
               .ToListAsync());
        }

        // GET: api/Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO_ReadUser>> GetUser(int id)
        {
          if (_context.Users == null)
          {
              return NotFound();
          }
            var user = await _context.Users.Include(user => user.Topics)
               .Include(user => user.RSVPs)
               .Include(user => user.Groups)
               .FirstOrDefaultAsync(u => u.Id == id);

            if (user == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadUser>(user);
        }

        // PUT: api/Users/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, DTO_EditUser editUser)
        {
            if (id != editUser.DTO_ID)
            {
                return BadRequest();
            }

            User DomainUser = await _context.Users.Include(g => g.Groups).Include(t => t.Topics).Include(r => r.RSVPs).FirstOrDefaultAsync(u => u.Id == editUser.DTO_ID);

            List<Group> tempGroups = new();

            foreach (int groupId in editUser.Groups)
            {
                if (await _context.Groups.FindAsync(groupId) == null)
                {
                    return BadRequest("Could not find one of the specified groups");
                }
                else
                {
                    Group group = await _context.Groups.FindAsync(groupId);
                    tempGroups.Add(group);
                }
            }

            List<RSVP> tempRSVP = new();

            foreach (int rsvpID in editUser.RSVPs)
            {
                if (await _context.RSVPs.FindAsync(rsvpID) == null)
                {
                    return BadRequest("Could not find one of the specified RSVPs");
                }
                else
                {
                    RSVP rsvp = await _context.RSVPs.FindAsync(rsvpID);
                    tempRSVP.Add(rsvp);
                }
            }

            List<Topic> tempTopics = new();

            foreach (int topicID in editUser.Topics)
            {
                if (await _context.Topics.FindAsync(topicID) == null)
                {
                    return BadRequest("Could not find one of the specified Topics");
                }
                else
                {
                    Topic topic = await _context.Topics.FindAsync(topicID);
                    tempTopics.Add(topic);
                }
            }



           

            DomainUser.Topics = tempTopics;
            DomainUser.RSVPs = tempRSVP;
            DomainUser.Groups = tempGroups;
          
            DomainUser.Name = editUser.Name;
            DomainUser.Status = editUser.Status;
            DomainUser.Bio = editUser.Bio;
            DomainUser.Img = editUser.Img;
            DomainUser.FunFact = editUser.FunFact;

            _context.Entry(DomainUser).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Users
        [HttpPost]
        public async Task<ActionResult<DTO_CreateUser>> PostUser(DTO_CreateUser userDTO)
        {
          if (_context.Users == null)
          {
              return Problem("Entity set 'AlumniContext.Users'  is null.");
          }
            User domainUser = _mapper.Map<User>(userDTO);
            await _context.Users.AddAsync(domainUser);
            
            await _context.SaveChangesAsync();

           return CreatedAtAction("GetUser", new { id = domainUser.Id}, userDTO);
        }

        //[HttpPut("~/character/joingroup")]

        //public async Task<ActionResult> JoinGroup(DTO_UserJoinGroup edituser, int userId, int GroupId)
        //{
        //    if (await _context.Users.FindAsync(userId) == null)
        //    {
        //        return BadRequest("User does not exist");
        //    }

        //    User DomainUser = await _context.Users.Include(g => g.Groups).FirstOrDefaultAsync(u => u.Id == userId);

        //    foreach (int groupId in edituser.Groups)
        //    {
        //        if (await _context.Groups.FindAsync(groupId) == null)
        //        {
        //            return BadRequest("Could not find one of the specified groups");
        //        }
        //        else if (DomainUser.Id(userId) == null) { }
        //        else
        //        {
        //            Group group = await _context.Groups.FindAsync(groupId);
        //            tempGroups.Add(group);
        //        }
        //    }

        //    foreach (int groupId in edituser.Groups)
        //    {
        //        if (await _context.Groups.FindAsync(groupId) == null)
        //        {
        //            return BadRequest("Could not find one of the specified groups");
        //        }
        //        else if (tempGroups.Any(group => group.Id == groupId))
        //        {

        //    var Group = await _context.Groups.FindAsync(userId);

        //    if (Group == null)
        //    {
        //        return BadRequest();
        //    }

        //    DomainUser.Groups.Add(Group);

        //    await _context.SaveChangesAsync();

        //    return NoContent();
        //}

        // DELETE: api/Users/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            if (_context.Users == null)
            {
                return NotFound();
            }
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool UserExists(int id)
        {
            return (_context.Users?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
