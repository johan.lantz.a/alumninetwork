﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkAPI.Data;
using AlumniNetworkAPI.Models;
using AlumniNetworkAPI.DTO.RsvpDTO;
using AutoMapper;
using Microsoft.AspNetCore.Cors;

namespace AlumniNetworkAPI.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class RSVPsController : ControllerBase
    {
        private readonly AlumniContext _context;
        private readonly IMapper _mapper;

        public RSVPsController(AlumniContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/RSVPs
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO_ReadRSVP>>> GetRSVPs()
        {
          if (_context.RSVPs == null)
          {
              return NotFound();
          }
            return _mapper.Map<List<DTO_ReadRSVP>>(await _context.RSVPs.ToListAsync());
        }

        // GET: api/RSVPs/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO_ReadRSVP>> GetRSVP(int id)
        {
          if (_context.RSVPs == null)
          {
              return NotFound();
          }
            var rsvp = await _context.RSVPs.FirstOrDefaultAsync(r => r.Id == id);

            if (rsvp == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadRSVP>(rsvp);
        }

        // PUT: api/RSVPs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRSVP(int id, DTO_EditRSVP rsvpEdit)
        {
            if (id != rsvpEdit.DTO_ID)
            {
                return BadRequest();
            }

            RSVP domainRSVP = await _context.RSVPs.FirstOrDefaultAsync(_r => _r.Id == rsvpEdit.DTO_ID);

            domainRSVP.LastUpdated = rsvpEdit.LastUpdated;
            domainRSVP.Accepted = rsvpEdit.Accepted;

            _context.Entry(domainRSVP).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RSVPExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RSVPs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTO_CreateRSVP>> PostRSVP(DTO_CreateRSVP rsvpDTO)
        {
          if (_context.RSVPs == null)
          {
              return Problem("Entity set 'AlumniContext.RSVPs'  is null.");
          }
           RSVP rsvp = _mapper.Map<RSVP>(rsvpDTO);
            
            await _context.RSVPs.AddAsync(rsvp);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRSVP", new { id = rsvp.Id }, rsvpDTO);
        }

        // DELETE: api/RSVPs/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRSVP(int id)
        {
            if (_context.RSVPs == null)
            {
                return NotFound();
            }
            var rSVP = await _context.RSVPs.FindAsync(id);
            if (rSVP == null)
            {
                return NotFound();
            }

            _context.RSVPs.Remove(rSVP);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool RSVPExists(int id)
        {
            return (_context.RSVPs?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
