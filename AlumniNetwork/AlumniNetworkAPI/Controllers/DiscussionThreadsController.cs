﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkAPI.Data;
using AlumniNetworkAPI.Models;
using AutoMapper;
using AlumniNetworkAPI.DTO.DiscussionThreadDTO;
using Microsoft.AspNetCore.Cors;

namespace AlumniNetworkAPI.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class DiscussionThreadsController : ControllerBase
    {
        private readonly AlumniContext _context;
        private readonly IMapper _mapper;

        public DiscussionThreadsController(AlumniContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/DiscussionThreads
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO_ReadDiscussionThread>>> GetThreads()
        {
            if (_context.Threads == null)
            {
                return NotFound();
            }
            return _mapper.Map<List<DTO_ReadDiscussionThread>>(await _context.Threads.ToListAsync());
        }

        // GET: api/DiscussionThreads/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO_ReadDiscussionThread>> GetDiscussionThread(int id)
        {
            if (_context.Threads == null)
            {
                return NotFound();
            }
            var discussionThread = await _context.Threads.Include(t => t.Posts).FirstOrDefaultAsync(t => t.Id == id);

            if (discussionThread == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadDiscussionThread>(discussionThread);
        }

        // PUT: api/DiscussionThreads/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDiscussionThread(int id, DTO_EditDiscussionThread editThread)
        {
            if (id != editThread.DTO_ID)
            {
                return BadRequest();
            }

            DiscussionThread currentThread = await _context.Threads.Include(t => t.Posts).FirstOrDefaultAsync(t => t.Id == editThread.DTO_ID);

            List<Post> tempPosts = new();

            foreach (int postId in editThread.Posts)
            {
                if (await _context.RSVPs.FindAsync(postId) == null)
                {
                    return BadRequest("One of the specified posts does not exist");
                }
                else
                {
                    Post post = await _context.Posts.FindAsync(postId);
                    tempPosts.Add(post);
                }
            }
            currentThread.Posts = tempPosts;
            currentThread.LastUpdatedAt = editThread.LastUpdatedAt;
            currentThread.IsEvent = editThread.IsEvent;

            _context.Entry(currentThread).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DiscussionThreadExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DiscussionThreads
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTO_CreateDiscussionThread>> PostDiscussionThread(DTO_CreateDiscussionThread threadDTO)
        {
            if (_context.Threads == null)
            {
                return Problem("Entity set 'AlumniContext.Threads'  is null.");
            }

            DiscussionThread domainDiscussionThread = _mapper.Map<DiscussionThread>(threadDTO);
            domainDiscussionThread.CreatedAt = DateTime.Now;
            

            await _context.Threads.AddAsync(domainDiscussionThread);

            await _context.SaveChangesAsync();

            Post post = _mapper.Map<Post>(threadDTO.TopLevelPost);
            
            post.CreatedAt = DateTime.Now;
            post.ThreadId = domainDiscussionThread.Id;
            post.CreatedBy = domainDiscussionThread.CreatedBy;

            await _context.Posts.AddAsync(post);

            await _context.SaveChangesAsync();


            return CreatedAtAction("GetDiscussionThread", new { id = domainDiscussionThread.Id }, threadDTO);
        }

        // DELETE: api/DiscussionThreads/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDiscussionThread(int id)
        {
            if (_context.Threads == null)
            {
                return NotFound();
            }
            var discussionThread = await _context.Threads.FindAsync(id);
            if (discussionThread == null)
            {
                return NotFound();
            }

            _context.Threads.Remove(discussionThread);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool DiscussionThreadExists(int id)
        {
            return (_context.Threads?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
