﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkAPI.Data;
using AlumniNetworkAPI.Models;
using AutoMapper;
using AlumniNetworkAPI.DTO.EventDTO;
using Microsoft.AspNetCore.Cors;

namespace AlumniNetworkAPI.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly AlumniContext _context;
        private readonly IMapper _mapper;

        public EventsController(AlumniContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Events
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO_ReadEvent>>> GetEvents()
        {
          if (_context.Events == null)
          {
              return NotFound();
          }
            return _mapper.Map<List<DTO_ReadEvent>>(await _context.Events.Include(@event => @event.RSVPs).ToListAsync());
        }

        // GET: api/Events/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO_ReadEvent>> GetEvent(int id)
        {
          if (_context.Events == null)
          {
              return NotFound();
          }
            var @event = await _context.Events.Include(e => e.RSVPs).FirstOrDefaultAsync(e => e.Id == id);

            if (@event == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadEvent>(@event);
        }

        // PUT: api/Events/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEvent(int id, DTO_EditEvent editEvent)
        {
            if (id != editEvent.DTO_ID)
            {
                return BadRequest();
            }

            Event currentEvent = await _context.Events.Include(e => e.RSVPs).FirstOrDefaultAsync(e => e.Id == id);

            List<RSVP> tempRSVP = new();

            foreach (int rsvpID in editEvent.RSVPs)
            {
                if (await _context.RSVPs.FindAsync(rsvpID) == null)
                {
                    return BadRequest("One of the specified RSVPs does not exist");
                }
                else
                {
                    RSVP rsvp = await _context.RSVPs.FindAsync(rsvpID);
                    tempRSVP.Add(rsvp);
                }
            }


            currentEvent.RSVPs = tempRSVP;

            currentEvent.Description = editEvent.Description;
            currentEvent.Name = editEvent.Name;
            currentEvent.BannerImg = editEvent.BannerImg;
            currentEvent.Start = editEvent.Start;
            currentEvent.End = editEvent.End;
            currentEvent.IsPrivate = editEvent.IsPrivate;
            currentEvent.ThreadId = editEvent.ThreadId;
            currentEvent.GroupId = editEvent.GroupId;
            currentEvent.LastUpdatedAt = editEvent.LastUpdatedAt;

            _context.Entry(currentEvent).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EventExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Events
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTO_CreateEvent>> PostEvent(DTO_CreateEvent eventDTO)
        {
          if (_context.Events == null)
          {
              return Problem("Entity set 'AlumniContext.Events'  is null.");
          }

          Event @event = _mapper.Map<Event>(eventDTO);
            
            await _context.Events.AddAsync(@event);
            await _context.SaveChangesAsync();
     

            return CreatedAtAction("GetEvent", new { id = @event.Id }, eventDTO);
        }

        // DELETE: api/Events/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEvent(int id)
        {
            if (_context.Events == null)
            {
                return NotFound();
            }
            var @event = await _context.Events.FindAsync(id);
            if (@event == null)
            {
                return NotFound();
            }

            _context.Events.Remove(@event);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool EventExists(int id)
        {
            return (_context.Events?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
