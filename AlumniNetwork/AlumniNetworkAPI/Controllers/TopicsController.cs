﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkAPI.Data;
using AlumniNetworkAPI.Models;
using AutoMapper;
using AlumniNetworkAPI.DTO.TopicDTO;
using Microsoft.AspNetCore.Cors;

namespace AlumniNetworkAPI.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class TopicsController : ControllerBase
    {
        private readonly AlumniContext _context;
        private readonly IMapper _mapper;

        public TopicsController(AlumniContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Topics
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO_ReadTopic>>> GetTopics()
        {
          if (_context.Topics == null)
          {
              return NotFound();
          }
            return _mapper.Map<List<DTO_ReadTopic>>(await _context.Topics.Include(topic => topic.Users).ToListAsync());
        }

        // GET: api/Topics/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO_ReadTopic>> GetTopic(int id)
        {
          if (_context.Topics == null)
          {
              return NotFound();
          }
            var topic = await _context.Topics.Include(topic => topic.Users).FirstOrDefaultAsync(t => t.Id == id);

            if (topic == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadTopic>(topic);
        }

        // PUT: api/Topics/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTopic(int id, DTO_EditTopic editTopic)
        {
            if (id != editTopic.DTO_ID)
            {
                return BadRequest();
            }

            Topic topic = await _context.Topics.Include(t => t.Users).FirstOrDefaultAsync(t => t.Id == editTopic.DTO_ID);

            List<User> tempUsers = new();

            foreach (int userId in editTopic.Users)
            {
                if (await _context.Users.FindAsync(userId) == null)
                {
                    return BadRequest("One of the specified user does not exist.");
                }
                else
                {
                    User user = await _context.Users.FindAsync(userId);
                    tempUsers.Add(user);
                }
            }

            topic.Name = editTopic.Name;
            topic.Description = editTopic.Description;
            topic.Users = tempUsers;

            _context.Entry(topic).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TopicExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Topics
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTO_CreateTopic>> PostTopic(DTO_CreateTopic topicDTO)
        {
          if (_context.Topics == null)
          {
              return Problem("Entity set 'AlumniContext.Topics'  is null.");
          }
            Topic topic = _mapper.Map<Topic>(topicDTO);
            await _context.Topics.AddAsync(topic);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTopic", new { id = topic.Id }, topicDTO);
        }

        // DELETE: api/Topics/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTopic(int id)
        {
            if (_context.Topics == null)
            {
                return NotFound();
            }
            var topic = await _context.Topics.FindAsync(id);
            if (topic == null)
            {
                return NotFound();
            }

            _context.Topics.Remove(topic);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool TopicExists(int id)
        {
            return (_context.Topics?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
