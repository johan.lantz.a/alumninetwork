﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkAPI.Data;
using AlumniNetworkAPI.Models;
using AutoMapper;
using AlumniNetworkAPI.DTO.PostDTO;
using Microsoft.AspNetCore.Cors;

namespace AlumniNetworkAPI.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly AlumniContext _context;
        private readonly IMapper _mapper;

        public PostsController(AlumniContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO_ReadPost>>> GetPosts()
        {
          if (_context.Posts == null)
          {
              return NotFound();
          }
            return _mapper.Map<List<DTO_ReadPost>>(await _context.Posts.ToListAsync());
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO_ReadPost>> GetPost(int id)
        {
          if (_context.Posts == null)
          {
              return NotFound();
          }
            var post = await _context.Posts.FirstOrDefaultAsync(p => p.Id == id);

            if (post == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadPost>(post);
        }

        // PUT: api/Posts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPost(int id, DTO_EditPost editPost)
        {
            if (id != editPost.DTO_ID)
            {
                return BadRequest();
            }

            Post domainPost = await _context.Posts.FirstOrDefaultAsync(p => p.Id == editPost.DTO_ID);

            domainPost.Text = editPost.Text;
            domainPost.LastUpdatedAt = editPost.LastUpdatedAt;

            
           _context.Entry(domainPost).State = EntityState.Modified;


            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PostExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Posts
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTO_CreatePost>> PostPost(DTO_CreatePost postDTO)
        {
          if (_context.Posts == null)
          {
              return Problem("Entity set 'AlumniContext.Posts'  is null.");
          }
            Post post = _mapper.Map<Post>(postDTO);
            await _context.Posts.AddAsync(post);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPost", new { id = post.Id }, postDTO);
        }

        // DELETE: api/Posts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePost(int id)
        {
            if (_context.Posts == null)
            {
                return NotFound();
            }
            var post = await _context.Posts.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            _context.Posts.Remove(post);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PostExists(int id)
        {
            return (_context.Posts?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
