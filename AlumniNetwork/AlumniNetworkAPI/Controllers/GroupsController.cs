﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AlumniNetworkAPI.Data;
using AlumniNetworkAPI.Models;
using AutoMapper;
using AlumniNetworkAPI.DTO.GroupDTO;
using Microsoft.AspNetCore.Cors;

namespace AlumniNetworkAPI.Controllers
{
    [EnableCors("AllowCors")]
    [Route("api/[controller]")]
    [ApiController]
    public class GroupsController : ControllerBase
    {
        private readonly AlumniContext _context;
        private readonly IMapper _mapper;

        public GroupsController(AlumniContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Groups
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DTO_ReadGroup>>> GetGroups()
        {
          if (_context.Groups == null)
          {
              return NotFound();
          }
            return _mapper.Map<List<DTO_ReadGroup>>(await _context.Groups.Include(groups => groups.Events)
                .Include(groups => groups.Users).ToListAsync());
        }

        // GET: api/Groups/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DTO_ReadGroup>> GetGroup(int id)
        {
          if (_context.Groups == null)
          {
              return NotFound();
          }
            var group = await _context.Groups.Include(groups => groups.Events)
                .Include(groups => groups.Users)
                .FirstOrDefaultAsync(g => g.Id == id);

            if (group == null)
            {
                return NotFound();
            }

            return _mapper.Map<DTO_ReadGroup>(group);
        }

        // PUT: api/Groups/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGroup(int id, DTO_EditGroup editGroup)
        {
            if (id != editGroup.DTO_Id)
            {
                return BadRequest();
            }

            Group group = await _context.Groups.Include(g => g.Events).Include(g => g.Users).FirstOrDefaultAsync(g => g.Id == editGroup.DTO_Id);

            List<User> tempUsers = new();

            foreach (int userId in editGroup.Users)
            {
                if (await _context.Users.FindAsync(userId) == null)
                {
                    return BadRequest("One of the specified users for this group does not exsist");
                }
                else
                {
                    User user = await _context.Users.FindAsync(userId);
                    tempUsers.Add(user);
                }
            }

            List<Event> tempEvents = new();

            foreach (int eventId in editGroup.Events)
            {
                if (await _context.Events.FindAsync(eventId) == null)
                {
                    return BadRequest("One of the specified events for this group does not exsist");
                }
                else
                {
                    Event @event = await _context.Events.FindAsync(eventId);
                    tempEvents.Add(@event);
                }
            }


            group.Events = tempEvents;
            group.Users = tempUsers;

            group.Description = editGroup.Description;
            group.Name = editGroup.Name;
            group.IsPrivate = editGroup.IsPrivate;

            _context.Entry(group).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GroupExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Groups
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<DTO_CreateGroup>> PostGroup(DTO_CreateGroup groupDTO)
        {
          if (_context.Groups == null)
          {
              return Problem("Entity set 'AlumniContext.Groups'  is null.");
          }
          Group group = _mapper.Map<Group>(groupDTO);
            await _context.Groups.AddAsync(group);
            
            await _context.SaveChangesAsync();


            return CreatedAtAction("GetGroup", new { id = group.Id }, groupDTO);
        }

        // DELETE: api/Groups/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteGroup(int id)
        {
            if (_context.Groups == null)
            {
                return NotFound();
            }
            var @group = await _context.Groups.FindAsync(id);
            if (@group == null)
            {
                return NotFound();
            }

            _context.Groups.Remove(@group);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool GroupExists(int id)
        {
            return (_context.Groups?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
